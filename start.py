import pickle
import numpy as np
from cnn import Cnn


class Starter:
    def __init__(self, new=0):
        if new is 1:
            self.cnn = Cnn(1,
                           [32, [20, 5, 1, 2], [15, 5, 1, 1], [20, 5, 1, 0], [10, 5, 1, 1], [16, 16, 20]])
        else:
            self.cnn = Cnn(0)

    def train(self):
        loss = []
        with open("training_data/cifar-100-python/train", 'rb') as file:
            dict = pickle.load(file, encoding='bytes')
        labels = dict[bytes('coarse_labels', 'utf8')]
        datas = dict[bytes('data', 'utf8')]
        for i in range(len(labels)):
            data = np.reshape(datas[i], (32, 32, 3)) / 255
            expected = np.zeros(20, dtype=int)
            expected[labels[i]] = 1
            self.cnn.forward(data)
            self.cnn.backprop(expected)
            if (i % 10) is 0:
                loss.append(self.eval())
            return 0

    def eval(self):
        with open("training_data/cifar-100-python/test", 'rb') as file:
            dict = pickle.load(file, encoding='bytes')
        labels = dict[bytes('coarse_labels', 'utf8')]
        datas = dict[bytes('data', 'utf8')]
        for i in range(len(labels)):
            data = np.reshape(datas[i], (32, 32, 3)) / 255
            expected = np.zeros(20, dtype=int)
            expected[labels[i]] = 1
            self.cnn.forward(data)
            return 0


Starter(1).train()
