import numpy as np


def normalize(data):
    output = []
    denom = 0
    for i in data:
        output.append(np.exp(i))
        denom = denom + np.exp(i)
    return [x / denom for x in output]


def softmax(actual, expected):
    index = 0
    normalized = normalize(actual)
    for i in range(len(expected)):
        index = i if (expected[i] == 1) else index
    return 0 - np.log10(normalized[index])


def d_softmax(actual, expected):
    print(actual)
    print(expected)
    deratives = list()
    denom = 0
    num_right = 0
    for i in range(len(actual)):
        denom = denom + np.exp(actual[i])
        if expected[i] == 1:
            num_right = np.exp(actual[i])
    for i in range(len(actual)):
        if expected[i] == 1:
            deratives.append(denom - num_right / (denom * np.log(10)))
        else:
            deratives.append(num_right / (denom * np.log(10)))
    return deratives


def sigmoid(z):
    return 1 / (1 + np.exp(z))


def relu(z):
    return np.maximum(0, z)


def leaky_relu(z):
    return np.maximum(np.multiply(0.001, z), np.multiply(0.1, z))


def d_leaky_relu(z, d):
    dz = list()
    for element in z:
        dz.append(0.1 if element > 0 else 0.001)
    return np.multiply(dz, d)
