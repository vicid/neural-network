import numpy as np
import activations as act
import serializer as ser


###
# a neural network with at least 1 hidden layer and 1 input and output layer
###
class FcLayer:
    def __init__(self, new, input_size, sizes, learn_rate=1e-4):
        self.path = "cnn/fclayer/" + str(new[1])
        if new[0] is 1:
            self.epoch = 0
            self.learnRate = learn_rate
            self.weights = []
            self.dweights = []
            self.dweights_momentum = []
            self.biases = []
            self.inputSize = input_size
            self.inputs = int(input_size[0] * input_size[0] * input_size[1])
            neurons = int(sizes[0])
            for layer in range(len(sizes)):
                self.biases.append(np.random.randn(neurons))
                self.weights.append(np.random.randn(neurons, self.inputs) /
                                    np.sqrt(neurons))
                self.dweights.append(np.zeros((neurons, self.inputs)))
                self.dweights_momentum.append(np.zeros((neurons, self.inputs)))
                if layer < len(sizes) - 1:
                    self.inputs = sizes[layer]
                    neurons = sizes[layer + 1]
            self.dbiases = np.zeros_like(self.biases)
            self.dbiases_momentum = np.copy(self.dbiases)
            ser.serialize(self, self.path)
        else:
            self = ser.de_serialize(self.path)

    ###
    # calculate the output from an input
    # IN: array of inputs
    ###
    def forward(self, data):
        dataT = list()
        for line in data:
            for column in line:
                for depth in column:
                    dataT.append(depth)
        self.data = dataT
        self.outputs = list()
        for layer in range(len(self.weights)):
            self.outputs.append(list())
            for neuron in self.weights[layer]:
                self.outputs[layer].append(np.dot(neuron, dataT))
            dataT = self.function(self.outputs[layer])
        return dataT

    def backward(self, expected):
        if self.epoch is 10:
            self.update()
            self.epoch = 0
        self.epoch = self.epoch + 1
        print('Loss: ', act.softmax(act.leaky_relu(self.outputs[-1]),
                                    expected))
        dstream = act.d_softmax(self.outputs[-1], expected)
        print('softmax deratives: ', dstream)
        for i in range(len(self.dweights) - 1, -1, -1):
            dstream = act.d_leaky_relu(self.outputs[i], dstream)
            self.dbiases[i] = np.add(self.dbiases[i], dstream)
            dstream_temp = np.zeros_like(self.weights[i][0])
            for j in range(int(len(dstream))):
                if i is 0:
                    self.dweights[i][j] = np.add(self.dweights[i][j],
                                                 np.multiply(dstream[j],
                                                             self.data))
                else:
                    self.dweights[i][j] = np.add(self.dweights[i][j],
                                                 np.multiply(dstream[j],
                                                             self.outputs[i - 1]))
                dstream_temp = np.add(dstream_temp, dstream[j] * self.weights[i][j])
            dstream = dstream_temp
        dstream_output = []
        print('dstream intern:', dstream)
        for line in range(self.inputSize[0]):
            dstream_output.append([])
            for column in range(self.inputSize[0]):
                dstream_output[line].append([])
                for depth in range(self.inputSize[1]):
                    position = (line + 1) * (column + 1) * (depth + 1)
                    dstream_output[line][column]
                    dstream[position - 1]
                    dstream_output[line][column].append(dstream[position - 1])
        return dstream_output

    def update(self):
        print('\n\nUpdate\n\n')
        self.dweights_momentum = np.add(np.multiply(0.9,
                                                    self.dweights_momentum),
                                        np.divide(self.dweights, self.epoch))
        self.weights = np.subtract(self.weights, self.learnRate * self.dweights_momentum)
        self.dbiases_momentum = np.add(0.9 * self.dbiases_momentum, self.dbiases / self.epoch)
        self.biases = np.subtract(self.biases, self.learnRate
                                  * self.dbiases_momentum)
        for i in range(len(self.dweights)):
            self.dweights[i] = np.zeros_like(self.dweights[i])
        self.dbiases = 0 * self.dbiases
        ser.serialize(self, self.path)

    def function(self, input):
        return act.leaky_relu(input)

    def d_function(self, input):
        pass

x = 0
if x is not 0:
    data = np.random.randn(5, 5, 1)
    fclayer = FcLayer([1, 1], [5, 1], [3, 4])
    weights = fclayer.weights

    print('\n\n\nSTART\n\n\n')
    for i in range(12):
        print('weights: \n\n', fclayer.weights, end='\n')
        print('biases: \n\n', fclayer.biases, end='\n')
        print('dweights: \n\n', fclayer.dweights, end='\n')
        print('dbiases: \n\n', fclayer.dbiases, end='\n')
        output = fclayer.forward(data)
        print('output: \n\n', output)
        print('dstream: \n\n', fclayer.backward([0, 0, 1, 0]), end='\n')
        print('weights changed', weights != fclayer.weights)
