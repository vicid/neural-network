import pickle
import os


def serialize(data, path):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'wb+') as file:
        pickle.dump(data, file)


def de_serialize(path):
    with open(path, 'rb') as file:
        return pickle.dump(file)
