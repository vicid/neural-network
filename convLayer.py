import numpy as np
import activations as act
import serializer as ser


###
#
# A Convolution Layer
# methods: forward and backward
#
###
class ConvLayer:
    ###
    # new = [a,b] -> a = [0,1] -> 0 = no, 1 = yes
    #                b = x, any number for saving/loading
    # input_size = [a,b] -> a = width, height
    #                       b = depth
    # architecture = [a,b,c,d] -> a = number of filter
    #                             b = width/height of filter
    #                             c = strides
    #                             d = padding
    # learn_rate = between 1e-3 and 1e-5
    ###
    def __init__(self, new, input_size, architecture, learn_rate=1e-3):
        self.path = 'cnn/convLayer/' + str(new[1])
        if new[0] is 1:
            self.inputSize = input_size
            self.filter_size = architecture[0:2]
            self.strides = architecture[2]
            self.padd = architecture[3]
            self.learnRate = learn_rate
            self.filters = np.random.randn(self.filter_size[0], self.filter_size[1],
                                           self.filter_size[1], self.inputSize[1])
            self.dfilters = np.zeros((self.filter_size[0], self.filter_size[1],
                                      self.filter_size[1], self.inputSize[1]))
            self.dfilters_momentum = np.zeros((self.filter_size[0], self.filter_size[1],
                                               self.filter_size[1], self.inputSize[1]))
            self.biases = np.random.randn(self.filter_size[0])
            self.dbiases = np.zeros(self.filter_size[0])
            self.dbiases_momentum = np.zeros(self.filter_size[0])
            self.d_map = np.zeros((self.inputSize[0], self.inputSize[0],
                                   self.inputSize[1]))
            ser.serialize(self, self.path)
        else:
            self = ser.de_serialize(self.path)

    ###
    # forward: forwarding data through the Convolution Layer
    # IN: data = [h*[w*[d]]] -> h = height
    #                           w = width
    #                           d = depth
    # OUT: data = [h*[w*[d]]] -> h = height
    #                            w = width 
    #                            d = depth
    ###
    def forward(self, data):
        print(self.path)
        print("datalen= ", len(data))
        self.data = data
        start = self.padd
        end = len(data) - 1 - self.padd
        self.activation_map = list()
        for i in range(start, end + 1, self.strides):
            lines = list()
            for j in range(start, end + 1, self.strides):
                lines.append(self.__apply_filters__(i, j))
            self.activation_map.append(lines)
        return self.__function__(self.activation_map)

    ###
    # backward: backpropagation through the Convolution Layer
    # IN: d_map_up = [h*[w*[d]]] -> h = height
    #                               w = width
    #                               d = depth
    # OUT:d_map_up = [h*[w*[d]]] -> h = height
    #                               w = width
    #                               d = depth   
    ###
    def backward(self, d_map_up):
        d_map_up = self.__d_function__(d_map_up)
        for line in range(len(d_map_up)):
            for column in range(len(d_map_up)):
                for filter_num in range(self.filter_size[0]):
                    for line_fil in range(-int(((self.filter_size[1] - 1) / 2)), int((self.filter_size[1] + 1) / 2)):
                        for column_fil in range(-int(((self.filter_size[1] - 1) / 2)),
                                                int((self.filter_size[1] + 1) / 2)):
                            x = line + line_fil + self.padd
                            y = column + column_fil + self.padd
                            if x >= 0 and y >= 0 and x < len(self.d_map) and y < len(self.d_map):
                                d_array = np.multiply(d_map_up[line][column][filter_num],
                                                      self.filters[filter_num][line_fil][column_fil])
                                self.d_map[x][y] = np.add(self.d_map[x][y], d_array)
                                d_array = np.multiply(d_map_up[line][column][filter_num],
                                                      self.data[x][y])
                                self.dfilters[filter_num][line_fil][column_fil] = np.add(
                                    self.dfilters[filter_num][line_fil][column_fil], d_array)
        return self.d_map

    ###
    # applying filter on a specific location of the data
    # IN: line = line of the data
    #     column = column of the data
    # OUT: output
    ###
    def __apply_filters__(self, line, column):
        pos_xy = list()
        for filter in self.filters:
            calc_filter = 0
            for i in range(self.filter_size[1]):
                i_dat = int(line - (self.filter_size[1] - 1 / 2) + i)
                for j in range(self.filter_size[1]):
                    j_dat = int(column - (self.filter_size[1] - 1 / 2) + j)
                    if (i_dat >= 0 and i_dat < len(self.data) and j_dat >= 0 and j_dat < len(self.data)):
                        calc_filter = calc_filter + np.dot(self.data[i_dat][j_dat],
                                                           filter[i][j])
            pos_xy.append(calc_filter)
        return pos_xy

    ###
    # an output function
    # IN: input
    # OUT: output
    ###
    def __function__(self, input):
        return act.leaky_relu(input)

    ###
    # a derivative function, corresponding to the function()
    # IN: input
    # OUT: deravitive
    ###
    def __d_function__(self, input):
        return input
        # return act.d_leaky_relu(self.activation_map, input)


x = 0
if x is not 0:
    data = np.random.randn(5, 5, 3)
    ddata = np.random.randn(5, 5, 5)
    conv1 = ConvLayer([1, 1], [5, 3], [5, 5, 1, 0])
    print('Filter:\n\n', conv1.filters)
    print('Forward:\n\n', conv1.forward(data))
    print('Backward:\n\n', conv1.backward(ddata))
    print('dFilter:\n\n', conv1.dfilters)
    ddata = np.random.randn(1, 1, 5)
    conv2 = ConvLayer([1, 1], [5, 3], [5, 5, 1, 2])
    print('Filter:\n\n', conv1.filters)
    print('Forward:\n\n', conv2.forward(data))
    print('Backward:\n\n', conv2.backward(ddata))
    print('dFilter:\n\n', conv2.dfilters)
