from convLayer import ConvLayer
from fcLayer import FcLayer
import serializer


class Cnn:
    def __init__(self, new, architecture):
        self.path = 'cnn/cnn'
        if new is 1:
            input_size = [architecture[0], 3]
            self.architecture = len(architecture) - 2
            self.cnn = []
            for layer in range(1, len(architecture) - 1):
                self.cnn.append(ConvLayer([1, layer], input_size, architecture[layer]))
                input_size[0] = int((input_size[0] - 2 * architecture[layer][-1] + architecture[layer][2] - 1) / \
                                    architecture[layer][2])
                input_size[1] = architecture[layer][0]
            self.cnn.append(FcLayer([1, 1], input_size, architecture[-1]))
            serializer.serialize(self.architecture, self.path)
        else:
            self.architecture = serializer.deSerialize(self.path)
            for i in range(1, self.architecture + 1):
                self.cnn.append(ConvLayer([0, i]))
            self.cnn.append(FcLayer([0, 1]))

    def forward(self, data):
        for layer in self.cnn:
            print('run #', layer, end='\n')
            data = layer.forward(data)

    def backprop(self, label):
        for layer in self.cnn[::-1]:
            label = layer.backward(label)
